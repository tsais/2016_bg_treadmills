/*
JavaScript Document

FinalVadaszy
10/27/15

Sticky menu control
*/

var $LV = jQuery.noConflict();
//$LV('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">');


$LV(window).load(function(){
  var stickyBottom = $('#stickyBottom').height();
  $LV('#stickyBottom').css({'position':'fixed','bottom':stickyBottom});

  $LV(window).scroll(function()
    $LV('#stickyBottom').css('bottom','0');
  });
});